#!/usr/bin/env python3
from setuptools import (
    setup,
    find_packages
)


setup(
    name='rabbit_rpc',
    version_format='{tag}.dev{commitcount}+{gitsha}',
    author='Author Name Placeholder',
    author_email='author.email@example.com',
    package_dir={'': 'src'},
    packages=find_packages('src'),
    setup_requires=['pytest-runner', 'setuptools-git-version'],
    tests_require=['pytest'],
    extras_require={
        'docs': [
            'sphinx',
            'sphinxcontrib-napoleon'
        ],
        'test': [
            'pytest',
            'pytest-runner'
        ]
    }
)
