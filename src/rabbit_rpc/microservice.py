#!/usr/bin/env python3
import logging
import json
import inspect

import pika


LOGGER = logging.getLogger(__name__)


def rpc(procedure):
    procedure.is_rpc = (callable(procedure) and
                        inspect.iscoroutinefunction(procedure))
    return procedure


class RabbitRPC:
    """Base asynchronous *RPC* service class.

    This service declares methods for connecting, reconnecting
    and closing connection with **RabbitMQ** broaker.
    """

    def __init__(self, connection_params, name=None,
                 exchange='', queue=None, routing_key=None,
                 exchange_type='direct', queue_params=None,
                 exchange_params=None):
        """ RPC constuctor.

        Args:
            connection_params (pika.ConnectionParameters): Connection
                                                           parameters
            name (str, optional): Service name, this name could be used to
                                  generate routing key.
            exchange (str, optional): Rpc service exchange name, defaults to ''
            queue (str, optional): Rpc service listenning queue name, defaults
                                   to service name.
            routing_key (str|types.NoneType, optional): Routing key for
                                                        incomming queue,
                                                        this key could be
                                                        generated from service
                                                        name.
            exchange_type (str, optional): Exchange type, defaults to 'direct'.
                                           Options are
                                           *{'direct', 'fanout', 'topic'}*.
            queue_params (dict|types.NoneType, optional): Declared incomming
                                                          queue parameters.
                                                          Defaults to:
                                                          *{'durable': True,*
                                                          *'exclusive': False,*
                                                          *'auto_delete':*
                                                          *False}*
            exchange_params (dict|types.NoneType, optional): Exchange
                                                             parameters.

        Examples:

            .. code-block:: py

                class SimpleGreeter(RabbitRPC):

                    def __init__(self, *args, **kwargs):
                        super().__init__(*args, **kwargs)

                    @rpc
                    async def greet(self, service):
                        return f"Hello {service}"

                greeter = SimpleGreeter(params)

                greeter.start()

        """
        assert connection_params is not None, "Connection parameters must" + \
                                              " be defined"
        self._connection_params = connection_params
        self._name = name or self.__class__.__name__
        self._exchange = exchange
        self._queue = queue or self._name
        self._queue_params = queue_params or {'durable': True,
                                              'exclusive': False,
                                              'auto_delete': False}
        self._exchange_params = exchange_params or {
            'passive': False,
            'durable': True,
            'auto_delete': False
        }
        if 'exchange_type' not in self._exchange_params:
            self._exchange_params['exchange_type'] = exchange_type
        self._routing_key = routing_key or '.'.join(
            [ident for ident in (self._exchange, self._queue) if ident]
        )
        self._connection = None
        self._channel = None
        self._consumer_tag = None
        self._stopping = False

    def _connect(self):
        LOGGER.info(
            "Initializing pika connection to RabbitMQ `%s:%u`",
            self._connection_params.host,
            self._connection_params.port
        )
        self._connection = pika.SelectConnection(
            self._connection_params,
            on_open_callback=self._on_connection_open,
            on_close_callback=self._on_connection_close,
            stop_ioloop_on_close=False
        )
        LOGGER.info("Connection initialized asynchronously ... ")
        return self._connection

    def _on_connection_open(self, connection):
        """ Callback called after pika opens it's connection to RabbitMQ.

        Args:
            connection (pika.SelectConnection): Open amqp connection object
        """
        LOGGER.info("Connection initialized ...")
        LOGGER.debug("Adding `_on_connection_close` callback ...")
        self._connection.add_on_close_callback(self._on_connection_close)
        LOGGER.info("Opening channel")
        connection.channel(self._on_channel_open)

    def _on_connection_close(self, _connection, reply_code, reply_text):

        self._channel = None
        if self._stopping:
            self._connection.ioloop.stop()
        else:
            LOGGER.warning(
                'Connection closed, reopening in 5 seconds: (%s) %s',
                reply_code,
                reply_text
            )
            self._connection.add_timeout(5, self._reconnect)

    def _reconnect(self):
        self._connection.ioloop.stop()

        if not self._stopping:
            self._connect()

            self._connection.ioloop.start()

    def start(self):
        """ Starts *Rpc service*, by connecting to **RabbitMQ** and declaring
            all necessary exchanges and queues. This method will start event
            loop, so it will block until service is closed.
        """
        self._connect()
        self._connection.ioloop.start()

    def close(self):
        if self._connection is not None:
            self._connection.close()
            self._connection.ioloop.start()

    def _on_channel_open(self, channel):
        """ Callback called when the channel is created.

        Args:
            channel (pika.channel.Channel): RabbitMQ channel.

        """
        LOGGER.info("Channel `%d` in open: [ %s ] ...",
                    channel.channel_number,
                    channel.is_open())
        self._channel = channel
        self._channel.add_on_close_callback(self._on_channel_close)
        self._setup_exchange()

    def _on_channel_close(self, channel, reply_code, reply_text):
        LOGGER.warning(
            "Channel `%i` was closed: (%s) %s",
            channel,
            reply_code,
            reply_text
        )
        self._connection.close()

    def _setup_exchange(self):
        LOGGER.info("Setting up exchange `%s`: `%r`",
                    self._exchange, self._exchange_params)
        self._channel.exchange_declare(
            self._on_exchange_declareok,
            self._exchange,
            **self._exchange_params
        )

    def _on_exchange_declareok(self, _frame):
        LOGGER.info("Exchange `%s` declared ...", self._exchange)
        self._setup_queue()

    def _setup_queue(self):
        LOGGER.info("Declaring queue `%s`", self._queue)

        self._channel.queue_declare(
            self._on_queue_declareok,
            self._queue,
            **self._queue_params
        )

    def _on_queue_declareok(self, method_frame):
        LOGGER.info("Queue `%s` declared ...", self._queue)
        LOGGER.info(
            "Binding queue `%s` to exchange `%s` with routing key: `%s`",
            self._queue,
            self._exchange,
            self._routing_key
        )
        self._channel.queue_bind(self._on_bindok, self._queue,
                                 self._exchange, self._routing_key)

    def _on_bindok(self):
        LOGGER.info("Queue `%s` bound to exchange `%s`",
                    self._queue, self._exchange)
        self._start_consuming()

    def _start_consuming(self):
        def _on_consumer_canceled(method_frame):
            LOGGER.info("Consumer was canceled remotely, shutting down `%r`",
                        method_frame)
            self._channel.cancel()

        self._channel.add_on_cancel_callback(_on_consumer_canceled)

        self._consumer_tag = self._channel.basic_consume(
            self._consume_message,
            self._queue
        )

    async def _consume_message(self, channel, basic_deliver, properties, body):
        """ Asynchronousli called consumer method

        Args:
            channel (pika.channel.Channel): Consumer channel
            basic_deliver (pika.spec.Basic.Deliver): basic deliver method
            properties (pika.spec.BasicProperties): properties
            body (bytes): Encoded message
        """
        LOGGER.debug("Received message # %s from %s",
                     basic_deliver.delivery_tag,
                     properties.app_id)
        hdrs = properties.headers or {}
        procedure_name = hdrs.get('procedure', '_not_defined')
        routing_key = hdrs['routing_key']
        try:

            decoded_body = body.decode('utf-8')

            message = json.loads(decoded_body)

            args = message.get('args', [])
            kwargs = message.get('kwargs', {})

            result = await self._call_procedure(procedure_name, args, kwargs)

            await self._reply(result, routing_key, properties.app_id)
        except InvalidInputException as exc:
            await self._reply_exc(exc, routing_key, properties.app_id)
        finally:
            await channel.basic_ack(basic_deliver.delivery_tag)

    async def _reply(self, result, back_routing_key, app_id):
        pass

    async def _reply_exc(self, exc, back_routing_key, app_id):
        pass

    async def call_remote(self, namespace, procedure, args, kwargs):
        pass

    async def _call_procedure(self, procedure_name, args, kwargs):
        logging.debug(
            "Called procedure `%s` with args: `%s` and kwargs: `%s`",
            procedure_name,
            repr(args),
            repr(kwargs)
        )
        procedure = getattr(self, procedure_name, None)

        if callable(procedure) and getattr(procedure, 'is_rpc', False):
            try:
                return await procedure(*args, **kwargs)
            except Exception as exc:
                logging.exception("Procedure `%s` failed during execution,"
                                  " cause: "
                                  " `[%s] %s`",
                                  procedure.__name__,
                                  type(exc),
                                  str(exc))
                raise
        else:
            raise InvalidInputException(("Called `{}.{}` does not exists,"
                                         " or is not a procedure").format(
                                            self._name, procedure_name
                                         ))

    @rpc
    async def health(self):
        """Check health status of service.

        Returns:
            dict: Basic health status data.

        Examples:
            .. code-block:: json

                {
                    "queued": 10,
                    "avg_completion_time": 0.01,
                    "last_completion_time": 1.1123,
                    "server": {
                        "utilization": 0.31,
                        "iops": 101002,
                    },
                    "errors": 101293,
                    "input_errors": 99123,
                    "service_uptime": 432525.123,
                    "health_check_took": 0.45,
                }

        """
        return {
            'queued': 10,
            'last_procedure_processing_time': 1.2213,
            'average_procedure_processing_time': 0.1123
        }


class InvalidInputException(Exception):
    pass
